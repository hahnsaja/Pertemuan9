package dao;

import com.mysql.cj.api.jdbc.Statement;
import model.Person;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hahn on 16/03/17.
 * diambil dari buku Java Desktop by Ifnu Bima
 */
public class PersonDaoJdbc {

    private Connection connection;
    private PreparedStatement insertStatement;
    private PreparedStatement updateStatement;
    private PreparedStatement deleteStatement;
    private PreparedStatement getAllStatement;
    private PreparedStatement getByIdStatement;
    private PreparedStatement getIdByNameStatement;

    private final String insertQuery = "insert into T_PERSON(name,password) " +
            " values(?,?)";
    private final String updateQuery = "update T_PERSON set name=?, " +
            " password=? where id=?";
    private final String deleteQuery = "delete from T_PERSON where id=?";
    private final String getByIdQuery = "select * from T_PERSON where id =?";
    private final String getAllQuery = "select * from T_PERSON";
    private final String getIdByNameQuery = "SELECT * from T_PERSON WHERE name=?";

    public void setConnection(Connection connection) throws SQLException {
        this.connection = connection;
        insertStatement = this.connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
        updateStatement = this.connection.prepareStatement(updateQuery);
        deleteStatement = this.connection.prepareStatement(deleteQuery);
        getByIdStatement = this.connection.prepareStatement(getByIdQuery);
        getAllStatement = this.connection.prepareStatement(getAllQuery);
        getIdByNameStatement = this.connection.prepareStatement(getIdByNameQuery);
    }

    public Person save(Person person) throws SQLException{
        if (person.getId() == null) {
            insertStatement.setString(1, person.getName());
            insertStatement.setString(2, person.getPassword());
            Long xid = (long) insertStatement.executeUpdate();
//            ambil id
            ResultSet rs = insertStatement.getGeneratedKeys();
            if(rs.next()){
                Long id = rs.getLong(1);
                person.setId(id);
            }
//            person.setId(getIdByName(person.getName()));
        } else {
            updateStatement.setString(1, person.getName());
            updateStatement.setString(2, person.getPassword());
            updateStatement.setLong(3, person.getId());
            updateStatement.executeUpdate();
        }
        return person;
    }

    public Person delete(Person person) throws SQLException{
        deleteStatement.setLong(1, person.getId());
        deleteStatement.executeUpdate();
        return person;
    }

    public Person getById(Long id) throws SQLException{
        getByIdStatement.setLong(1, id);
        ResultSet rs = getByIdStatement.executeQuery();
        //proses mapping dari relational ke object
        if (rs.next()) {
            Person person = new Person();
            person.setId(rs.getLong("id"));
            person.setName(rs.getString("name"));
            person.setPassword(rs.getString("password"));
            return person;
        }
        return null;
    }

    public List<Person> getAll() throws SQLException{
        List<Person> persons = new ArrayList<>();
        ResultSet rs = getAllStatement.executeQuery();
        while(rs.next()){
            Person person = new Person();
            person.setId(rs.getLong("id"));
            person.setName(rs.getString("name"));
            person.setPassword(rs.getString("password"));
            persons.add(person);
        }
        return persons;
    }

    public Long getIdByName(String name) throws SQLException{
        getIdByNameStatement.setString(1, name);
        ResultSet rs = getIdByNameStatement.executeQuery();
        if(rs.next()){
            Long id = rs.getLong("id");
            return id;
        }
        return null;
    }

}
