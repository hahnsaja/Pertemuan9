package service;


import dao.PersonDaoJdbc;
import model.Person;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by hahn on 16/03/17.
 * diambil dari buku Java Desktop by Ifnu Bima
 */
public class ServiceJdbc {


    private PersonDaoJdbc personDao;
    private Connection connection;
    public void setDataSource(DataSource dataSource){
        try {
            connection = dataSource.getConnection();
            personDao = new PersonDaoJdbc();
            personDao.setConnection(connection);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public Person save(Person person){
        try {
            //set id
            connection.setAutoCommit(false);
            Long id = personDao.getIdByName(person.getName());
            connection.commit();
            connection.setAutoCommit(true);
            if(id != null){
                person.setId(id);
                return person;
            }

            connection.setAutoCommit(false);
            personDao.save(person);
            connection.commit();
            connection.setAutoCommit(true);

        } catch (SQLException ex) {
            try{
                connection.rollback();
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
        return person;
    }
    public Person delete(Person person){
        try {
            connection.setAutoCommit(false);
            personDao.save(person);
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException ex) {
            try{
                connection.rollback();
            }catch(SQLException e){
                e.printStackTrace();
            }
        }
        return person;
    }
    public Person getPerson(Long id){
        try {
            return personDao.getById(id);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
