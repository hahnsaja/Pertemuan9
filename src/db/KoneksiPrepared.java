package db;

import java.sql.*;

/**
 * Created by hahn on 14.03.17.
 */
public class KoneksiPrepared extends KoneksiDb {

    public void selectData(String name, String password){
        super.openDb();
        PreparedStatement ps;
        String sql = "Select * from T_PERSON where name=? and password=?";
        try {
            ps = super.getConnection().prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            if(!rs.isBeforeFirst()){
                System.out.println("Kosong\n");
                return;
            }
            while (rs.next()){
                System.out.print("ID: " + rs.getString("id"));
                System.out.print(" | nama: " + rs.getString("name"));
                System.out.println("\t| Pass:  " + rs.getString("password"));
            }

        } catch (SQLException e){
            e.printStackTrace();
        }
        super.closeDb();
    }



}
