package db;

import java.sql.*;

/**
 * Created by hahn on 17.03.17.
 */
public class KoneksiTransactionQuery {

    private static String confTimeZone = "serverTimezone=UTC";

    //sesuaikan nama database dengan database milik anda
    private static String url = "jdbc:mysql://localhost:3306/latihan_oop?" + confTimeZone;
    private static String user = "root";
    private static String password = "";//kalau pakai password, isi di sini

    private static Connection connection;
    private static Statement statement;
    private static ResultSet result;
    private static PreparedStatement ps;


    private static void openDb(){
        try {
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void closeDb(){
        try {
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void insertTransaction(int num){
        PreparedStatement statement;
        String sql = "INSERT into T_PERSON(name, password) VALUES (?,?)";
        openDb();
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(sql);
            for(int i=0;i<num;i++){
                statement.setString(1, "user ke-" + i);
                statement.setString(2, "password ke-" + i);
                statement.addBatch();
            }
            statement.executeBatch();
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }

            e.printStackTrace();
        }
        closeDb();

    }

    public static void deleteData(){
        String sql = "DELETE FROM T_PERSON WHERE name LIKE '%administrator%'";
        PreparedStatement ps;
        openDb();
        try {
            connection.setAutoCommit(false);
            ps = connection.prepareStatement(sql);
            ps.executeUpdate(sql);
            connection.commit();
            connection.setAutoCommit(true);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeDb();
    }

    public static void selectAllData(){
        openDb();
        String sql = "Select * from T_PERSON";
        try {
            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            if(!rs.isBeforeFirst()){
                System.out.println("Kosong\n");
                return;
            }
            while (rs.next()){
                System.out.print("ID: " + rs.getString("id"));
                System.out.print(" | nama: " + rs.getString("name"));
                System.out.println("\t| Pass:  " + rs.getString("password"));
            }

        } catch (SQLException e){
            e.printStackTrace();
        }
        closeDb();
    }

    public static void main(String[] args){

        int num = 10;
        System.out.println("Query Pakai batch & transaction sebanyak " + num + " kali.");
//        insertTransaction(num);

//        selectAllData();

        System.out.println("Datanya dihapus lagi.");
        deleteData();
        selectAllData();
    }
}
