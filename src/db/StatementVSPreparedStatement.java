package db;

import java.sql.*;

/**
 * Created by hahn on 17.03.17.
 */
public class StatementVSPreparedStatement {

    private static String confTimeZone = "serverTimezone=UTC";

    //sesuaikan nama database dengan database milik anda
    private static String url = "jdbc:mysql://localhost:3306/latihan_oop?" + confTimeZone;
    private static String user = "root";
    private static String password = "";//kalau pakai password, isi di sini

    private static Connection connection;
    private static Statement statement;
    private static ResultSet result;
    private static PreparedStatement ps;


    private static void openDb(){
        try {
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void closeDb(){
        try {
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    //pakai statement biasa
    public static void selectData(String name, String password){
        String sql = "SELECT * FROM T_PERSON where name='" + name + "' and password='" + password + "';";
        System.out.println(sql);
        openDb();
        try {
            result = statement.executeQuery(sql);
            if(!result.isBeforeFirst()){
                System.out.println("Kosong");
                return;

            }
            while (result.next()){
                System.out.print("ID: " + result.getString("id"));
                System.out.print(" | nama: " + result.getString("name"));
                System.out.println("\t| Pass:  " + result.getString("password"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeDb();
    }

    public static void selectDataPrepared(String name, String password){
        openDb();
        String sql = "Select * from T_PERSON where name=? and password=?";
        try {
            PreparedStatement ps;
            ps = connection.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            if(!rs.isBeforeFirst()){
                System.out.println("Kosong\n");
                return;
            }
            while (rs.next()){
                System.out.print("ID: " + rs.getString("id"));
                System.out.print(" | nama: " + rs.getString("name"));
                System.out.println("\t| Pass:  " + rs.getString("password"));
            }

        } catch (SQLException e){
            e.printStackTrace();
        }
        closeDb();
    }

    public static void main(String[] args){

        System.out.println("Pakai Statement biasa");
        selectData("Hacker' or 1=1--'", "passwordsalah' or 1=1 --'");

        System.out.println("Pakai Prepared Statement");
        selectDataPrepared("Hacker' or 1=1--'", "passwordsalah' or 1=1 --'");



    }


}
