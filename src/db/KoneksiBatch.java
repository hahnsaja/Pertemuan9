package db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by hahn on 16/03/17.
 */
public class KoneksiBatch extends KoneksiDb{

    public void insertBatch(int num){
        PreparedStatement statement;
        String sql = "INSERT into T_PERSON(name, password) VALUES (?,?)";
        super.openDb();
        try {
            statement = super.getConnection().prepareStatement(sql);
            for(int i=0;i<num;i++){
                statement.setString(1, "user ke-" + i);
                statement.setString(2, "password ke-" + i);
                statement.addBatch();
            }
            statement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.closeDb();

    }

    public void deleteData(){
        String sql = "DELETE FROM T_PERSON WHERE name LIKE '%user ke%'";
        PreparedStatement ps;
        super.openDb();
        try {
            ps = super.getConnection().prepareStatement(sql);
            ps.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.closeDb();
    }
}
