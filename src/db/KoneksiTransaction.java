package db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by hahn on 16/03/17.
 */
public class KoneksiTransaction extends KoneksiDb {

    public void insertTransaction(int num){

        PreparedStatement statement;
        String sql = "INSERT into T_PERSON(name, password) VALUES (?,?)";
        super.openDb();
        try {
            super.getConnection().setAutoCommit(false);
            statement = super.getConnection().prepareStatement(sql);
            for(int i=0;i<num;i++){
                statement.setString(1, "user ke-" + i);
                statement.setString(2, "password ke-" + i);
                statement.addBatch();
            }
            statement.executeBatch();
            super.getConnection().commit();
            super.getConnection().setAutoCommit(true);

        } catch (SQLException e) {
            try {
                super.getConnection().rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
        super.closeDb();

    }
}
