package db;

import java.sql.*;

/**
 * Created by hahn on 17.03.17.
 */
public class KoneksiStatementSQLInjection {

    private static String confTimeZone = "serverTimezone=UTC";

    //sesuaikan nama database dengan database milik anda
    private static String url = "jdbc:mysql://localhost:3306/latihan_oop?" + confTimeZone;
    private static String user = "root";
    private static String password = "";//kalau pakai password, isi di sini

    private static Connection connection;
    private static Statement statement;
    private static ResultSet result;


    private static void openDb(){
        try {
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void closeDb(){
        try {
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void selectData(String name, String password){
//        String sql = "SELECT * FROM T_PERSON where name='hacker' or 1=1 -- and password='bypass'";
        String sql = "SELECT * FROM T_PERSON where name='" + name + "' and password='" + password + "';";
        System.out.println(sql);
        openDb();
        try {
            result = statement.executeQuery(sql);
            if(!result.isBeforeFirst()){
                System.out.println("Kosong");
                return;

            }
            while (result.next()){
                System.out.print("ID: " + result.getString("id"));
                System.out.print(" | nama: " + result.getString("name"));
                System.out.println("\t| Pass:  " + result.getString("password"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeDb();
    }

    public static void main(String args[]){

        //tanpa sql injection
        System.out.println("Tanpa sql injection");
        selectData("Anto", "passwordsalah");

        //sql injection
        System.out.println("Coba sql injection");
        selectData("Hacker' or 1=1--'", "passwordsalah' or 1=1 --'");


    }
}
