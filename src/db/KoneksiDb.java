package db;

import java.sql.*;

/**
 * Created by hahn on 14.03.17.
 */
public class KoneksiDb {

    private String confTimeZone = "serverTimezone=UTC";
    private String url = "jdbc:mysql://localhost:3306/latihan_oop?" + confTimeZone;
    private String user = "root";
    private String password = "";

    private Connection connection;
    private Statement statement;
    private ResultSet rs;

    protected void openDb(){
        try {
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void closeDb(){
        try {
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
