package db;

import java.sql.*;

/**
 * Created by hahn on 14.03.17.
 */
public class KoneksiStatement {

    private String confTimeZone = "serverTimezone=UTC";
    private String url = "jdbc:mysql://localhost:3306/latihan_oop?" + confTimeZone;
    private String user = "root";
    private String password = "";

    private Connection connection;
    private Statement statement;
    private ResultSet result;

    private void openDb(){
        try {
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void closeDb(){
        try {
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void selectData(){
        String sql = "SELECT * FROM T_PERSON where name='hacker' or 1=1 -- and password='bypass'";
        String sqlw = "SELECT * FROM T_PERSON where name='Anto' and password='bypass'";
        openDb();
//        System.out.println();
        try {
            result = statement.executeQuery(sql);
            if(!result.isBeforeFirst()){
                System.out.println("Kosong");
                return;

            }
            while (result.next()){
                System.out.print("ID: " + result.getString("id"));
                System.out.print(" | nama: " + result.getString("name"));
                System.out.println("\t| Pass:  " + result.getString("password"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeDb();
    }

    public void selectData(String name, String password){
//        String sql = "SELECT * FROM T_PERSON where name='hacker' or 1=1 -- and password='bypass'";
        String sql = "SELECT * FROM T_PERSON where name='" + name + "' and password='" + password + "';";
        System.out.println(sql);
        openDb();
        try {
            result = statement.executeQuery(sql);
            if(!result.isBeforeFirst()){
                System.out.println("Kosong");
                return;

            }
            while (result.next()){
                System.out.print("ID: " + result.getString("id"));
                System.out.print(" | nama: " + result.getString("name"));
                System.out.println("\t| Pass:  " + result.getString("password"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeDb();
    }
}
