package main;

import com.mysql.cj.jdbc.MysqlDataSource;
import db.KoneksiBatch;
import db.KoneksiPrepared;
import db.KoneksiStatement;
import db.KoneksiTransaction;
import model.Person;
import service.ServiceJdbc;

import java.sql.SQLException;

/**
 * Created by hahn on 14.03.17.
 */
public class Main {

    public static void main(String[] args){
        KoneksiStatement ks = new KoneksiStatement();
        KoneksiPrepared kp = new KoneksiPrepared();
        KoneksiBatch kb = new KoneksiBatch();
        KoneksiTransaction kt = new KoneksiTransaction();

//        ks.selectData();

//        kp.selectData("Anto", "anto");
//        kp.selectData("Anto' or 1=1 --'", "'anto");
//
//        System.out.println("Pakai Statement biasa");
//        ks.selectData("Anto", "antos");
//        ks.selectData("Anto' or 1=1 --'", "antos' or 1=1 --'");


        //koneksi batch
//        kb.insertBatch(10);
//        ks.selectData();

        //delete data
        kb.deleteData();
//        ks.selectData();

        //transaksi
//        kt.insertTransaction(10);
//        ks.selectData();

        //cara dao
//        caraDao();
    }

    private static void caraDao(){

        //buka koneksi
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUser("root");
        dataSource.setPassword("");
        dataSource.setDatabaseName("latihan_oop?serverTimezone=UTC");
        dataSource.setServerName("localhost");

        dataSource.setPortNumber(3306);

        //service db
        ServiceJdbc service = new ServiceJdbc();
        service.setDataSource(dataSource);

        Person person = new Person();
        person.setName("administrator 19");
        person.setPassword("pwd");
        person = service.save(person);

        System.out.println("id : " + person.getId());
        System.out.println("name: " + person.getName());
        try {
            dataSource.getConnection().close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
